<?php
/**
 * Created by PhpStorm.
 * User: hamza
 * Date: 17-7-22
 * Time: 3:46 PM
 */

namespace Modules\Wordpress\Repositories;


use Modules\Wordpress\Entities\Site;
use Modules\Wordpress\Contracts\SiteInterface;

class SiteRepository implements SiteInterface
{
    public $site;
    protected $user;

    function __construct(Site $site)
    {
        $this->site = $site;

    }


    public function all($columns = array('*'))
    {
        return $this->site->all($columns);
    }

    public function paginate($perPage = 15, $columns = array('*'))
    {
        $user = \Auth::user();
        return $this->site
            ->where('user_id', $user->id)
            ->with('category')
            ->paginate($perPage,$columns);
    }
    public function trash($perPage = 15, $columns = array('*'))
    {
        $user = \Auth::user();
        return $this->site::onlyTrashed()
            ->where('user_id', $user->id)
            ->with('category')
            ->paginate($perPage);
    }

    public function create(array $data)
    {
        $data['user_id']= \Auth::user()->id;
        return $this->site->create($data);
    }

    public function update(array $data, $id, $attribute = "id")
    {
        $user = \Auth::user();
        return $this->site
            ->where('user_id',$user->id)
            ->where($attribute, '=', $id)
            ->update($data);
    }

    public function delete($id)
    {
        $user = \Auth::user();
        return $this->site->where('user_id',$user->id)->delete($id);
    }

    public function forceDelete($id)
    {
        $user = \Auth::user();

        return $this->site::onlyTrashed()->where('user_id', $user->id)->forceDelete($id);
    }

    public function find($id, $columns = array('*'))
    {
        $user = \Auth::user();
        return $this->site->where('user_id',$user->id)->find($id, $columns);
    }
    public function findInTrash($id, $columns = array('*'))
    {
        $user = \Auth::user();
        return $this->site::onlyTrashed()->where('user_id',$user->id)->find($id, $columns);
    }

    public function findBy($attribute, $value, $columns = array('*'))
    {
        $user = \Auth::user();
        return $this->site->where('user_id',$user->id)->where($attribute, '=', $value)->first($columns);
    }
}