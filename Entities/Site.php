<?php

namespace Modules\Wordpress\Entities;

use App\Entities\CommonCategory;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Modules\Wordpress\Entities\Site
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $category_id
 * @property bool $active
 * @property string $username
 * @property string $password
 * @property string $domain
 * @property string|null $description
 * @property string|null $logo
 * @property string|null $about
 * @property string|null $contact
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|Site whereAbout($value)
 * @method static Builder|Site whereActive($value)
 * @method static Builder|Site whereCategoryId($value)
 * @method static Builder|Site whereContact($value)
 * @method static Builder|Site whereCreatedAt($value)
 * @method static Builder|Site whereDeletedAt($value)
 * @method static Builder|Site whereDescription($value)
 * @method static Builder|Site whereDomain($value)
 * @method static Builder|Site whereId($value)
 * @method static Builder|Site whereLogo($value)
 * @method static Builder|Site whereName($value)
 * @method static Builder|Site wherePassword($value)
 * @method static Builder|Site whereUpdatedAt($value)
 * @method static Builder|Site whereUserId($value)
 * @method static Builder|Site whereUsername($value)
 * @mixin \Eloquent
 */
class Site extends \Eloquent
{
    use SoftDeletes;
    protected $table = 'wordpress_sites';
    protected $fillable = [
        'name',
        'user_id',
        'category_id',
        'active',
        'username',
        'password',
        'domain',
        'description',
        'logo',
        'about',
        'contact'
    ];
    protected $hidden = ['password'];


    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function category()
    {
        return $this->hasOne(CommonCategory::class, 'id', 'category_id');
    }

}
