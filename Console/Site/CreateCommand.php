<?php

namespace Modules\Wordpress\Console\Site;

use Illuminate\Console\Command;
use Modules\Wordpress\Repositories\SiteRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'site:create {name} {domain} {username} {password} {category} {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create wordpress site';

    /**
     * User repository to persist user in database
     *
     * @var SiteRepository
     */
    protected $siteRepository;

    /**
     * Create a new command instance.
     *
     * @param SiteRepository $siteRepository
     * @internal param SiteRepository $userRepository
     */
    public function __construct(SiteRepository $siteRepository)
    {
        parent::__construct();
        $this->siteRepository = $siteRepository;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        //{name} {domain} {username} {password} {category} {user}
        return [
            ['name', InputArgument::REQUIRED, 'wordpress '],
            ['domain', InputArgument::REQUIRED, 'wordpress.com '],
            ['username', InputArgument::REQUIRED, 'wordpress@example.com'],
            ['password', InputArgument::REQUIRED, 'P@ssw0rd'],
            ['category', InputArgument::REQUIRED, '10'],
            ['user', InputArgument::REQUIRED, '20'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
