<?php
/**
 * Created by PhpStorm.
 * User: hamza
 * Date: 17-7-18
 * Time: 4:34 PM
 */
return [
    'site_create_title'=>'New Site Created',
    'site_update_title'=>'Site Updated',
    'site_trash_title'=>'Site Moved to Trash ',
    'site_deleted_title'=>'Site Deleted ',
    'site_create_message'=>'Successfully, :NAME site was created',
    'site_update_message'=>'Successfully, :NAME site was updated',
    'site_trash_message'=>'Successfully, :NAME site was moved to trash',
    'site_delete_message'=>'Successfully, :NAME site was deleted'
];