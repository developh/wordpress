<?php
/**
 * Created by PhpStorm.
 * User: hamza
 * Date: 17-7-17
 * Time: 10:50 AM
 */
return [
    'dashboard'=>'Dashboard',
    'manage_sites'=>'Manage Sites',
    'manage_categories'=>'Manage Categories',
    'manage_tags'=>'Manage Tags',
    'manage_posts'=>'Manage Posts',
    'reports'=>'Reports',
    'settings'=>'Settings'
];