<?php
/**
 * Created by PhpStorm.
 * User: hamza
 * Date: 17-7-18
 * Time: 4:34 PM
 */
return [
    'manage_your_websites'=>'Manage your wordpress websites',
    'wordpress_websites'=>'Wordpress Websites',
    'column_name'=>'Name',
    'column_category'=>'Category',
    'column_active'=>'Active',
    'column_inactive'=>'Inactive',
    'column_username'=>'Username',
    'column_domain'=>'Domain',
    'column_description'=>'Description',
    'column_password'=>'Password',
    'column_contact'=>'Contact Link',
    'column_about'=>'About Link',
    'column_logo'=>'Website Logo',
    'column_actions'=>'Actions',
    'total_pages'=>'Total Pages',
    'total_rows'=>'Total Sites',
    'current_page'=>'Current Page',
    'add_new_site'=>'Add New Site',
    'view_site_trash'=>'View Site Trash',
    'view_site_home'=>'Go Site Home',
];