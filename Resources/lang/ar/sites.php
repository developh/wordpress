<?php
/**
 * Created by PhpStorm.
 * User: hamza
 * Date: 17-7-18
 * Time: 4:34 PM
 */
return [
    'manage_your_websites'=>'Manage your wordpress websites',
    'wordpress_websites'=>'Wordpress Websites',
    'column_name'=>'Name',
    'column_category'=>'Category',
    'column_active'=>'Active',
    'column_username'=>'Username',
    'column_domain'=>'Domain',
    'column_description'=>'Description',
];