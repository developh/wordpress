<!-- .multi -->
<li class="selected">
    <a href="javascript:void(0)">
        <i class="fa fa-wordpress"></i>
    </a>
    <div class="sidebarmenu">
        <h3 class="menu-title text-danger">{!! __("wordpress::general.wordpress") !!}</h3>
        <div class="searchable-menu">
            <form role="search" id="wordpress-search" class="menu-search">
                <input name="keyword" placeholder="Search..." class="form-control">
                <a href="javascript:void(0)" onclick="$('#wordpress-search').submit()">
                    <i class="fa fa-search"></i>
                </a>
            </form>
        </div>
        <ul class="sidebar-menu">
            <li><a href="{!! url('/wordpress') !!}">{!! __("wordpress::menu.dashboard") !!}</a></li>
            <li><a href="{!! url('/wordpress/site') !!}">{!! __("wordpress::menu.manage_sites") !!}</a></li>
            <li><a href="{!! url('/wordpress/post') !!}">{!! __("wordpress::menu.manage_posts") !!}</a></li>
            <li><a href="{!! url('/wordpress/categories') !!}">{!! __("wordpress::menu.manage_categories") !!}</a></li>
            <li><a href="{!! url('/wordpress/tags') !!}">{!! __("wordpress::menu.manage_tags") !!}</a></li>
            <li><a href="{!! url('/wordpress/reports') !!}">{!! __("wordpress::menu.reports") !!}</a></li>
            <li><a href="{!! url('/wordpress/settings') !!}">{!! __("wordpress::menu.settings") !!}</a></li>
        </ul>
        <!-- Left navbar-header end -->
    </div>
</li>
<!-- /.multi -->