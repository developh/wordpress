@extends('wordpress::layouts.master')

@section('content')
    <div class="col-sm-12">
        <div class="white-box">
            <div class="box-tools pull-right">
                <button type="button" id="add" title="{!! __('wordpress::sites.add_new_site') !!}" class="btn btn-info btn-circle "><i class="fa fa-plus"></i> </button>
                <button type="button" id="trash" title="{!! __('wordpress::sites.view_trash') !!}" class="btn btn-warning btn-circle "><i class="fa fa-trash"></i> </button>
                <button type="button" id="home" title="{!! __('wordpress::sites.view_site_home') !!}" class="btn btn-success btn-circle "><i class="fa fa-home"></i> </button>
            </div>

            <h3 class="box-title m-b-0">{!! __('wordpress::sites.wordpress_websites') !!}</h3>
            <p class="text-muted m-b-30">{!! __('wordpress::sites.manage_your_websites') !!}</p>
            <table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable
                   data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                <thead>
                <tr>
                    <th scope="col" data-tablesaw-sortable-col
                        data-tablesaw-priority="persist">{!! __('wordpress::sites.column_name') !!}</th>
                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col
                        data-tablesaw-priority="3">{!! __('wordpress::sites.column_category') !!}</th>
                    <th scope="col" data-tablesaw-sortable-col
                        data-tablesaw-priority="3">{!! __('wordpress::sites.column_username') !!}</th>
                    <th scope="col" data-tablesaw-sortable-col
                        data-tablesaw-priority="4">{!! __('wordpress::sites.column_domain') !!}</th>
                    <th scope="col" data-tablesaw-sortable-col
                        data-tablesaw-priority="5">{!! __('wordpress::sites.column_description') !!}</th>
                    <th scope="col"
                        data-tablesaw-priority="6">{!! __('wordpress::sites.column_actions') !!}</th>

                </tr>
                </thead>
                <tbody id="data"></tbody>
            </table>
            <ul class="pager pull-right">
                <li><a href="javascript:void(0)" class="paginate" id="previous-page"><i class="fa fa-angle-left"></i> Previous</a></li>
                <li><a href="javascript:void(0)" class="paginate" id="next-page">Next <i class="fa fa-angle-right"></i></a></li>
                <li>{!! __('wordpress::sites.total_pages') !!}:<span id="total-pages"></span></li>
                <li>{!! __('wordpress::sites.total_rows') !!}:<span id="total-rows"></span></li>
                <li>{!! __('wordpress::sites.current_page') !!}:<span id="current-page"></span></li>
            </ul>
            <div class="clearfix"></div>
        </div>

    </div>
    <div id="site-edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                {!! Form::open(['url' => 'api/wordpress/site','enctype'=>'multipart/form-data']) !!}
                <input type="hidden" name="_method" value="put">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Site Details</h4>
                </div>
                <div class="modal-body">

                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="control-label">{!! __("wordpress::sites.column_name") !!}
                                    :*</label>
                                <input class="form-control" id="name" name="name">
                                <input type="hidden" class="form-control" id="id" name="id">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="category"
                                       class="control-label">{!! __('wordpress::sites.column_category') !!}:*</label>
                                <select class="form-control" id="category" name="category_id">
                                    <option value="1">Cat test</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="description"
                                       class="control-label">{!! __("wordpress::sites.column_description") !!}:</label>
                                <textarea rows=10 class="form-control" id="description" name="description"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="username"
                                       class="control-label">{!! __("wordpress::sites.column_username") !!}:*</label>
                                <input class="form-control" id="username" name="username">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password"
                                       class="control-label">{!! __("wordpress::sites.column_password") !!}:*</label>
                                <input class="form-control" id="password" name="password">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="domain" class="control-label">{!! __("wordpress::sites.column_domain") !!}
                                    :*</label>
                                <input class="form-control" id="domain" name="domain">
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12 p-t-20">
                        <div class="col-sm-2 ">
                            <div class="form-group">
                                <div class="radio radio-success">
                                    <input id="active" type="radio" value="1" name="active">
                                    <label for="checkbox-9"> {!! __('wordpress::sites.column_active') !!} </label>
                                </div>
                                <div class="radio radio-success">
                                    <input id="inactive" type="radio" value="0" name="active">
                                    <label for="checkbox-9"> {!! __('wordpress::sites.column_inactive') !!} </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="contact" class="control-label">{!! __("wordpress::sites.column_contact") !!}
                                    :</label>
                                <input class="form-control" id="contact" name="contact">
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="about" class="control-label">{!! __("wordpress::sites.column_about") !!}
                                    :</label>
                                <input class="form-control" id="about" name="about">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Save changes</button>
                </div>
                {!! Form::open()!!}
            </div>
        </div>
    </div>

@endsection


