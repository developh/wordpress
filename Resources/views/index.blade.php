@extends('wordpress::layouts.master')

@section('content')
    <!-- Page Content -->


            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">This view is loaded from module: {!! config('wordpress.name') !!}</h3>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        <!-- /.container-fluid -->
@endsection
