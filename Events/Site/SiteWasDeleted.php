<?php

namespace Modules\Wordpress\Site\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Wordpress\Entities\Site;

class SiteWasDeleted
{
    use SerializesModels;

    public $site;

    /**
     * Create a new event instance.
     *
     * @param Site $site
     */
    public function __construct(Site $site)
    {
        $this->site=$site;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
