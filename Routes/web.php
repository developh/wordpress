<?php

Route::group(['middleware' => 'web', 'prefix' => 'wordpress', 'namespace' => 'Modules\Wordpress\Http\Controllers'], function()
{
    Route::get('/', 'WordpressController@index');
    Route::get('/site', 'WordpressController@site');
    Route::get('/post', 'PostController@index');

});

