<?php

Route::group(['middleware' => 'web', 'prefix' => 'api/wordpress', 'namespace' => 'Modules\Wordpress\Http\Controllers'], function()
{
    Route::resource('site', 'Api\SiteController');
});

