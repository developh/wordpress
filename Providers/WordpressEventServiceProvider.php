<?php

namespace Modules\Wordpress\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Wordpress\Events\Site\SiteWasCreated;
use Modules\Wordpress\Events\Site\SiteWasMovedToTrash;
use Modules\Wordpress\Events\Site\SiteWasUpdated;
use Modules\Wordpress\Listeners\Site\SendSiteCreatedNotification;
use Modules\Wordpress\Listeners\Site\SendSiteMovedToTrashNotification;
use Modules\Wordpress\Listeners\Site\SendSiteUpdatedNotification;

class WordpressEventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        SiteWasCreated::class => [
            SendSiteCreatedNotification::class,
        ],
        SiteWasUpdated::class => [
            SendSiteUpdatedNotification::class,
        ],
        SiteWasMovedToTrash::class => [
            SendSiteMovedToTrashNotification::class,
        ],
    ];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
