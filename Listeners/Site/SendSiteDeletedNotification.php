<?php

namespace Modules\Wordpress\Site\Listeners;

use Auth;
use Modules\Wordpress\Events\events;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Wordpress\Site\Events\SiteWasDeleted;

class SendSiteDeletedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SiteWasDeleted $event
     * @return void
     */
    public function handle(SiteWasDeleted $event)
    {
        Auth::user()->notify(new NotifyUserOfSiteMovedToTrash($event->site));
    }
}
