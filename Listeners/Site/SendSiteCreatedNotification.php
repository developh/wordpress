<?php

namespace Modules\Wordpress\Listeners\Site;

use Auth;
use Modules\Wordpress\Events\Site\SiteWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Wordpress\Notifications\Site\NotifyUserOfCreatedSite;

class SendSiteCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SiteWasCreated $event
     * @return void
     */
    public function handle(SiteWasCreated $event)
    {
        Auth::user()->notify(new NotifyUserOfCreatedSite($event->site));
    }
}
