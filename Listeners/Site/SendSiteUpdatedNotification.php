<?php

namespace Modules\Wordpress\Listeners\Site;

use Auth;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Wordpress\Events\Site\SiteWasUpdated;
use Modules\Wordpress\Notifications\Site\NotifyUserOfUpdatedSite;

class SendSiteUpdatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SiteWasUpdated $event
     * @return void
     */
    public function handle(SiteWasUpdated $event)
    {
        Auth::user()->notify(new NotifyUserOfUpdatedSite($event->site));
    }
}
