<?php

namespace Modules\Wordpress\Listeners\Site;

use Auth;
use Modules\Wordpress\Events\Site\SiteWasMovedToTrash;
use Modules\Wordpress\Notifications\Site\NotifyUserOfSiteMovedToTrash;


class SendSiteMovedToTrashNotification
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SiteWasMovedToTrash $event
     * @return void
     */
    public function handle(SiteWasMovedToTrash $event)
    {
        Auth::user()->notify(new NotifyUserOfSiteMovedToTrash($event->site));
    }
}
