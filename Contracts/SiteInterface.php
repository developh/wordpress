<?php
/**
 * Created by PhpStorm.
 * User: hamza
 * Date: 17-7-22
 * Time: 3:43 PM
 */

namespace  Modules\Wordpress\Contracts;


Interface SiteInterface
{
    public function all($columns = array('*'));

    public function paginate($perPage = 15, $columns = array('*'));

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function forceDelete($id);

    public function trash($perPage = 15, $columns = array('*'));

    public function find($id, $columns = array('*'));

    public function findInTrash($id, $columns = array('*'));

    public function findBy($field, $value, $columns = array('*'));
}