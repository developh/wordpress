<?php

namespace Modules\Wordpress\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class WordpressController extends Controller
{
    protected $_breadcrumb = [];

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $breadcrumb = [
            [
                'name' => __('wordpress::general.wordpress'),
                'link' => url('wordpress'),
                'active' => false
            ],
            [
                'name' => __('wordpress::menu.dashboard'),
                'link' => url('wordpress'),
                'active' => true
            ]
        ];
        $data = [
            'breadcrumbs' => $breadcrumb,
            'page_title' => __('wordpress::menu.dashboard')
        ];

        return view('wordpress::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('wordpress::create');
    }


    public function site()
    {
        $breadcrumb = [
            [
                'name' => __('wordpress::general.wordpress'),
                'link' => url('wordpress'),
                'active' => false
            ],
            [
                'name' => __('wordpress::menu.manage_sites'),
                'link' => url('wordpress'),
                'active' => true
            ]
        ];
        $data = [
            'breadcrumbs' => $breadcrumb,
            'page_title' => __('wordpress::menu.dashboard')
        ];
        return view('wordpress::sites.index',$data);
    }


    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('wordpress::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('wordpress::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
