<?php

namespace Modules\Wordpress\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Wordpress\Entities\Site;
use Modules\Wordpress\Events\Site\SiteWasCreated;
use Modules\Wordpress\Events\Site\SiteWasMovedToTrash;
use Modules\Wordpress\Events\Site\SiteWasUpdated;
use Modules\Wordpress\Http\Requests\SiteRequest;
use Modules\Wordpress\Contracts\SiteInterface;

class SiteController extends Controller
{
    protected $site;

    public function __construct(SiteInterface $site)
    {
        $this->site = $site;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(!$request->get('trash',false)){
            return $this->site->paginate(4);
        }else{
            return $this->site->trash(4);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param SiteRequest $request
     * @return Response
     */
    public function create(SiteRequest $request)
    {
        $site=$this->site->create($request->toArray());
         \Event::fire(new SiteWasCrveated($this->site->find($site->id)));
        return Response(null,201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|SiteRequest $request
     * @return Response
     */
    public function store(SiteRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|SiteRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(SiteRequest $request, $id)
    {
        $this->site->update($request->all(),$id);
        \Event::fire(new SiteWasUpdated($this->site->find($id)));
       return Response(null,204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $site=$this->site->findInTrash($id);
        if($site instanceof Site){
            $this->site->forceDelete($id);
        }else{
            $site=$this->site->find($id);
            $this->site->delete($id);
        }

        \Event::fire(new SiteWasMovedToTrash($site));
        return Response(null,200);
    }

}
