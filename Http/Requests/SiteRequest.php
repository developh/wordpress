<?php

namespace Modules\Wordpress\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->offsetUnset('_method');
        $this->offsetUnset('_token');
        return [
            'name'=>'required',
            'category_id'=>'required',
            'active'=>'required|boolean',
            'username'=>'required',
            'password'=>'required',
            'domain'=>'required|url|active_url',
            'contact'=>'nullable|url|active_url',
            'about'=>'nullable|url|active_url',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
