<?php

namespace Modules\Wordpress\Notifications\Site;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Wordpress\Entities\Site;

class NotifyUserOfSiteMovedToTrash extends Notification implements ShouldQueue
{
    use Queueable;
    protected $site;

    /**
     * Create a new notification instance.
     *
     * @param Site $site
     */
    public function __construct(Site $site)
    {
        $this->site=$site;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $url="/wordpress/site/{$this->site->id}";
        return [
            'url'=>$url,
            'title'=>__('wordpress::notifications.site_trash_title'),
            'message'=>__('wordpress::notifications.site_trash_message',['name'=>$this->site->name]),
            'icon'=>'<i class="fa fa-wordpress trash notifications-icon"></i>',
            'action'=>'trash',
        ];
    }
}
