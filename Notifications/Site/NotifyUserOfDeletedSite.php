<?php

namespace Modules\Wordpress\Notifications\Site;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Wordpress\Entities\Site;

class NotifyUserOfDeletedSite extends Notification
{
    use Queueable;
    protected $site;

    /**
     * Create a new notification instance.
     *
     * @param Site $site
     */
    public function __construct(Site $site)
    {
        $this->site=$site;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $url="/wordpress/site/{$this->site->id}";
        return [
            'url'=>$url,
            'title'=>__('wordpress::notifications.site_deleted_title'),
            'message'=>__('wordpress::notifications.site_deleted_message',['name'=>$this->site->name]),
            'icon'=>'<i class="fa fa-wordpress delete notifications-icon"></i>',
            'action'=>'delete',
        ];
    }
}
