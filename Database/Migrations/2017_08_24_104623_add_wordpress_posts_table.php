<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWordpressPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wordpress_posts', function (Blueprint $table) {

            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->integer('post_parent_id')->unsigned();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('status_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->text('post_slug')->unique()->index();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('wordpress_posts_type');
            $table->foreign('status_id')->references('id')->on('wordpress_posts_status');
            $table->foreign('post_parent_id')->references('id')->on('wordpress_posts');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wordpress_posts');

    }
}
